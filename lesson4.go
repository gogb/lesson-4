package main

import (
	"fmt"
	"os"
	"sort"
)

// this is a comment

func main() {
	var cnt int
	fmt.Println("Введите количество чисел: ")
	_, err := fmt.Scanln(&cnt)

	if err != nil {
		fmt.Println("Некорректное число")
		os.Exit(1)
	}

	var a []int
	var b int
	for i := 0; i < cnt; i++ {
		fmt.Println("Введите число №", (i + 1), ": ")
		_, err := fmt.Scanln(&b)

		if err != nil {
			fmt.Println("Некорректное число")
			os.Exit(1)
		}
		a = append(a, b)
	}

	sort.Ints(a)
	fmt.Println(a)
}
